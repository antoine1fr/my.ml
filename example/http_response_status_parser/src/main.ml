module HttpStatus = struct
  type status_class =
    | Informational of int
    | Success of int
    | Redirection of int
    | Client_error of int
    | Server_error of int
    | Unknown

  type t = (string * status_class)

  let print = function
    | (http_version, Informational code) ->
      Printf.printf "%s - Informational (%d)\n" http_version code
    | (http_version, Success code) ->
      Printf.printf "%s - Success (%d)\n" http_version code
    | (http_version, Redirection code) ->
      Printf.printf "%s - Redirection (%d)\n" http_version code
    | (http_version, Client_error code) ->
      Printf.fprintf stderr "%s - Client error (%d)\n" http_version code
    | (http_version, Server_error code) ->
      Printf.fprintf stderr "%s - Server error (%d)\n" http_version code
    | (http_version, Unknown) ->
      Printf.fprintf stderr "%s - Unknown status\n" http_version

  module Parser = struct
    open My.Parser
    open My.Parser.Op
    open My.Parser.Text

    let crlf =
      char '\r' >>= fun cr ->
      char '\n' >>= fun lf ->
      return ((Char.escaped cr) ^ (Char.escaped lf))

    let status =
      let check_semantic http_version status_code =
        match status_code with
        | x when (x >= 100) && (x <= 199) -> (http_version, Informational x)
        | x when (x >= 200) && (x <= 299) -> (http_version, Success x)
        | x when (x >= 300) && (x <= 399) -> (http_version, Redirection x)
        | x when (x >= 400) && (x <= 499) -> (http_version, Client_error x)
        | x when (x >= 500) && (x <= 599) -> (http_version, Server_error x)
        | _ -> (http_version, Unknown)
      in
      string >>= fun http_version ->
      char ' ' >>= fun _ ->
      int >>= fun status_code ->
      char ' ' >>= fun _ ->
      read_until_newline >>= fun status_string ->
      crlf >>= fun _ ->
      check_semantic http_version status_code |> return

    let parse str =
      let (My.Parser.Parser parse') = status in
      let (value, _) = parse' (str, 0) in
      value
  end
end

let () =
  let status_list = [
    HttpStatus.Parser.parse "HTTP/1.1 200 Ok\r\n";
    HttpStatus.Parser.parse "HTTP/1.1 200 Ok\r";
    HttpStatus.Parser.parse "HTTP/1.1 200 Ok\n";
    HttpStatus.Parser.parse "HTTP/1.1 200 Ok";
    HttpStatus.Parser.parse "pouet";
    HttpStatus.Parser.parse "HTTP/1.0 404 Not found\r\n";
    HttpStatus.Parser.parse "HTTP/1.0 804 Dunno what I'm doing!\r\n"
  ] in
  List.iter (fun parse_result ->
    match parse_result with
    | None -> Printf.fprintf stderr "Syntax error.\n"
    | Some http_status -> HttpStatus.print http_status
  ) status_list
