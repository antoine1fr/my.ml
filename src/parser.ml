module Parser = struct
  type state = string * int
  type 'a t = Parser of (state -> ('a option * state))

  let return value = Parser (fun state -> (Some value, state))

  let bind (Parser parse) f = Parser (fun state ->
    match parse state with
    | (None, _) -> (None, state)
    | (Some value, state') ->
      let Parser parse' = f value in
      parse' state'
  )

  let next = Parser (fun (str, n) ->
    if n >= String.length str then
      (None, (str, n))
    else
      (Some str.[n], (str, n + 1))
  )

  let repeat_while predicate (Parser parse) = Parser (fun state ->
    let rec loop l s =
      match parse s with
      | (None, _) -> (None, s)
      | (Some value, state') ->
        match predicate value with
        | false -> (Some (List.rev l), s)
        | true -> loop (value :: l) state'
    in
    loop [] state
  )

  module Op = struct
    let ( >>= ) = bind
  end

  module Text = struct
    let char c = Parser (fun (str, n) ->
      let open Op in
      if (n < String.length str) && (c = str.[n]) then
        (Some str.[n], (str, n + 1))
      else
        (None, (str, n))
    )

    let string_of_char_list l =
      List.fold_left (fun str c -> str ^ (Char.escaped c)) "" l

    let int =
      let open Op in
      repeat_while (fun c ->
        match c with
        | '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' -> true
        | _ -> false
      ) next
      >>= fun char_list ->
        string_of_char_list char_list
        |> int_of_string
        |> return

    let read_until_newline =
      let open Op in
      repeat_while (fun c ->
        match c with
        | '\n' | '\r' -> false
        | _ -> true
      ) next
      >>= fun char_list ->
        string_of_char_list char_list
        |> return

    let string =
      let open Op in
      repeat_while (fun c ->
        match c with
        | '\n' | '\r' | ' ' | '\t' -> false
        | _ -> true
      ) next
      >>= fun char_list ->
        string_of_char_list char_list
        |> return

    let newline =
      let open Op in
      repeat_while (fun c ->
        match c with
        | '\n' | '\r' -> true
        | _ -> false
      ) next
      >>= fun char_list ->
        string_of_char_list char_list
        |> return
  end
end
