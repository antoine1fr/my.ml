TARGET = my.cmxa

COMPILER = ocamlopt
RM = rm -f

BUILD_FLAGS = -I src/
LINK_FLAGS = -a
DEPENDENCIES =

SRC_DIR = src/
SOURCES = \
		$(SRC_DIR)parser.ml \
		$(SRC_DIR)my.ml

O_FILES = $(SOURCES:.ml=.o)
CMX_FILES = $(SOURCES:.ml=.cmx)
CMI_FILES = $(SOURCES:.ml=.cmi)

$(TARGET): $(O_FILES) $(CMX_FILES)
	$(COMPILER) -o $(TARGET) $(LINK_FLAGS) $(DEPENDENCIES) $(CMX_FILES)

all: $(TARGET)

clean:
	$(RM) $(CMX_FILES)
	$(RM) $(CMI_FILES)
	$(RM) $(O_FILES)

fclean: clean
	$(RM) $(TARGET)

re: fclean all

.PHONY: all clean fclean re

%.cmx: %.ml
	$(COMPILER) -c $<

%.o: %.ml
	$(COMPILER) -o $@ $(BUILD_FLAGS) -c $<
